﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField]
    public TMP_Text score;
    [SerializeField]
    public int scoreValue;

    public static Score Instance
    {
        get; private set;
    }

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(gameObject);
        }
    }

    public int ScoreValue
    {
        get {
            return scoreValue;
        }
    }

    public void Start()
    {
        ResetScore();
    }

    // this methos is called at the inspector
    public void AddScore(string _value)
    {
        int value = int.Parse(_value);
        scoreValue += value;
        score.text = scoreValue.ToString();
    }

    public void ResetScore()
    {
        Debug.Log("RESET SCORE");
        scoreValue = 0;
        score.text = scoreValue.ToString();
    }
}
