﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [System.Serializable]
    private class RotationElements
    {
# pragma warning disable 0649
        public float speed;
        public float duration;
# pragma warning restore 0649
    }

    [SerializeField]
    private RotationElements rotationPattern;

    private WheelJoint2D wheelJoint2D;
    private JointMotor2D motor;
    private bool enableRotation;

    private void Awake()
    {
        wheelJoint2D = GetComponent<WheelJoint2D>();
        motor = new JointMotor2D();

    }

    public void InitializeRotation()
    {
        rotationPattern.duration = GameController.Instance.currentLevelData.rotationDuration;
        rotationPattern.speed = GameController.Instance.currentLevelData.rotationSpeed;
        enableRotation = true;
        StartCoroutine("StartRotation");
    }

    private IEnumerator StartRotation()
    {
        //int rotationIndex = 0;
        while(enableRotation)
        {
            yield return new WaitForFixedUpdate();

            motor.motorSpeed = rotationPattern.speed;
            motor.maxMotorTorque = 10000;

            wheelJoint2D.motor = motor;

            //yield return new WaitForSeconds(rotationPattern.duration);

            //rotationIndex = 0;
            //rotationIndex = rotationIndex < rotationPattern.Length ? rotationIndex : 0;
        }
    }

    public void StopRotation()
    {
        motor.motorSpeed = 0;
        Debug.Log("STOP ROTATION");
        enableRotation = false;
        StartCoroutine("StartRotation");
    }


}
