﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickSpawnerController :MonoBehaviour {
    [SerializeField]
    private int numberOfStickToSpawn;
    [SerializeField]
    private ObjectSpawner objectSpawner;
    [SerializeField]
    private GameObject rotatingObject;

    private int counter = 0;

    private void Awake() {
        if(objectSpawner == null)
            objectSpawner = GetComponent<ObjectSpawner>();
    }

    public void SpawnStick() {
        if(counter > 0) {
            objectSpawner.SpawnGameObject();
            counter--;
        } else {
            GameController.Instance.InitializeLevel();
        }
    }

    public void ResetStickValue() {
        numberOfStickToSpawn = GameController.Instance.currentLevelData.objectCount;
        counter = numberOfStickToSpawn;

        SpawnStick();

    }

    public void InitializeStick() {
        objectSpawner.DespawnAll();
    }
}
