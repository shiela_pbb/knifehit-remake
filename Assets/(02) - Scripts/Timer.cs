﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    public TMP_Text timer;
    [SerializeField]
    public float timerValue;
    [SerializeField]
    public Image timerDisplay;


    // Private Variables
    private float _timer;
    public float _timerValue;

    public void Awake()
    {
        _timerValue = timerValue;
        _timer = 1 / timerValue;
    }

    public void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        if(GameController.Instance.gameStatus == GameController.GameStatus.Playing)
        {
            timerValue -= Time.deltaTime;
            timer.text = Mathf.RoundToInt(timerValue).ToString();

            if(timerValue <= 0)
            {
                timer.text = "0";
                timerValue = 0;
                GameController.Instance.gameStatus = GameController.GameStatus.GameOver;
                GameController.Instance.GameOver();
            }

            SetTimerDisplayValue();
        }
    }

    public void DeductTime()
    {
        timerValue = timerValue - 0.5f;
        timer.text = timerValue.ToString();
    }

    public void ResetTimer()
    {
        Debug.Log("RESET TIMER");
        timerValue = _timerValue;

        SetTimerDisplayValue();
    }

    private void SetTimerDisplayValue()
    {
        timerDisplay.fillAmount = _timer * timerValue;
    }
}
