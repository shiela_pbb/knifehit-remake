﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickPreviewSpawner : MonoBehaviour
{
    // Public Variables
    public ObjectSpawner previewSpawner;
    public int numberOfPreviewObejctToSpawn;
    [SerializeField]
    private List<Vector3> previewPosition;


    public void Awake()
    {
        if(previewSpawner == null)
            previewSpawner = gameObject.GetComponent<ObjectSpawner>();
    }

    public void Start()
    {
        //InitializePreview();
    }

    public void InitializePreview()
    {
        Debug.Log("InitializePreview");
        previewSpawner.DespawnAll();
    }

    public void ResetPreview()
    {
        numberOfPreviewObejctToSpawn = GameController.Instance.currentLevelData.objectCount;

        for(int i = 0; i < numberOfPreviewObejctToSpawn; i++)
        {
            previewSpawner.prefabPosition = previewPosition[i];
            previewSpawner.SpawnGameObject();
        }
    }

    public void SpawnPreview()
    {
        previewSpawner.SpawnGameObject();
    }

    public void DespawnObject()
    {
        previewSpawner.DespawnGameobject();
    }
}
