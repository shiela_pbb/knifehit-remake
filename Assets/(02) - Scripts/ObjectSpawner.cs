﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    private GameObject prefab;

    public Vector3 prefabPosition;
    public Vector3 prefabRotation;
    [SerializeField]
    private int numberOfPrefabToSpawn;
    [SerializeField]
    private bool spawnAtStart;

    private Spawner spawner;

    private void Awake()
    {
        spawner = this.gameObject.GetComponent<Spawner>();
    }

    public void OnEnable()
    {
        spawner.SpawnAll(prefab, numberOfPrefabToSpawn, this.transform);
    }

    private void Start()
    {
        if (spawnAtStart)
            SpawnGameObject();
    }

    public void SpawnGameObject()
    {
        GameObject obj = spawner.Spawn();
        obj.transform.position = new Vector3(prefabPosition.x, prefabPosition.y, prefabPosition.z);
        //obj.transform.localRotation = new Quaternion(prefabRotation.x, prefabRotation.y, prefabRotation.z, 0f);
    }

    public void DespawnGameobject()
    {
        spawner.Despawn();
    }

    public void DespawnGameobject(GameObject obj)
    {
        spawner.DespawnGameObject(obj);
    }

    public void DespawnAll()
    {
        spawner.DespawnAll();
    }
}
