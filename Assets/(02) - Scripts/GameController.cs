﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using System.IO;

using Application = UnityEngine.Application;

public class GameController : MonoBehaviour
{
    public enum GameStatus
    {
        Start,
        Initializing,
        Playing,
        Pause,
        GameOver
    }

    [Serializable]
    public class CurrentLevelData
    {
        public int score;
        public Sprite stickSprite;
        public int objectCount;
        public Sprite rotatingSprite;
        public float rotationSpeed;
        public float rotationDuration;
    }

    public static GameController Instance { get; private set; }

 

    public Levels levels;
    public int currentLevelCount = 1;
    public TMP_Text level;
    public GameStatus gameStatus = GameStatus.Start;
    public CurrentLevelData currentLevelData;
    public UnityEvent initializeGame;
    public UnityEvent levelUp;
    public UnityEvent pauseGame;
    public UnityEvent resetGame;
    public UnityEvent resetLevel;
    public UnityEvent onGameOver;

    // Private Variables

    // TODO : method to catch message/event from onHideHUD. Start that game and set the game status to playing.
    // send event to spawner (1) and (2) start spawn
    // send event to rotating object to start the rotation

    // TODO : method to catch if out of time, Set the gameplay into game over.
    // not necessary this class, send event to onShowHUD. 

    void Awake()
    {
        if(Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }

    private void Start()  {
        Debug.Log("0 path:" + Application.dataPath);
        string ProjectPath = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
        string apkPath = Path.Combine(ProjectPath, Application.productName + ".apk");

        string buildPath = Path.Combine(apkPath);
        string exportPath = Path.GetFullPath(Path.Combine(ProjectPath, "../../android/UnityExport"));

        Debug.Log("1 ProjectPath:" + ProjectPath);
        Debug.Log("2 apkPath:" + apkPath);
        Debug.Log("3 buildPath:" + buildPath);
        Debug.Log("4 exportPath:" + exportPath);

        gameStatus = GameStatus.Start;
    }

    public void PopulateLevelData()
    {
        if(gameStatus == GameStatus.GameOver)
            return;

        if(currentLevelCount > levels.levels.Length)
        {
            currentLevelData.score = levels.levels[levels.levels.Length - 1 ].Score;
            currentLevelData.stickSprite = levels.levels[levels.levels.Length - 1].StickSprite;
            currentLevelData.objectCount = levels.levels[levels.levels.Length - 1].ObjectCount;
            currentLevelData.rotatingSprite = levels.levels[levels.levels.Length - 1].RotatingSprite;
            currentLevelData.rotationSpeed = levels.levels[levels.levels.Length - 1].RotationSpeed;
            currentLevelData.rotationDuration = levels.levels[levels.levels.Length - 1].RotationDuration;
        } else
        {
            currentLevelData.score = levels.levels[currentLevelCount - 1 ].Score;
            currentLevelData.stickSprite = levels.levels[currentLevelCount - 1].StickSprite;
            currentLevelData.objectCount = levels.levels[currentLevelCount - 1].ObjectCount;
            currentLevelData.rotatingSprite = levels.levels[currentLevelCount - 1].RotatingSprite;
            currentLevelData.rotationSpeed = levels.levels[currentLevelCount - 1].RotationSpeed;
            currentLevelData.rotationDuration = levels.levels[currentLevelCount - 1].RotationDuration;
        }
    }

    public void InitializeLevel()
    {
        gameStatus = GameStatus.Initializing;

        InitializeLevelData();

        // Check GameController gameobject for invoke event
        if(initializeGame != null)
            initializeGame.Invoke();
    }

    public void InitializeLevelData()
    {
        currentLevelCount++;
        level.text = currentLevelCount.ToString();

        PopulateLevelData();
    }


    public void ResetGame()
    {
        gameStatus = GameStatus.Start;

        currentLevelCount = 0;
        //level.text = currentLevelCount.ToString();

        InitializeLevel();

        // Check GameController gameobject for invoke event
        if(resetGame != null)
            resetGame.Invoke();
    }

    public void ResetLevel()
    {
        gameStatus = GameStatus.Initializing;

        level.text = currentLevelCount.ToString();
        PopulateLevelData();

        if(initializeGame != null)
            initializeGame.Invoke();

    }

    public void LevelUp()
    {
        if(gameStatus == GameStatus.Pause)
            return;
        // TODO : show level hud
        // Check GameController gameobject for invoke event
        if(levelUp != null)
            levelUp.Invoke();
    }

    public void OnCollisionWithSameObject()
    {
        Instance.gameStatus = GameStatus.Pause;
        Debug.Log("OnCollisionWithSameObject");

        // Check GameController gameobject for invoke event
        if(pauseGame != null)
            pauseGame.Invoke();
    }

    public void GameOver()
    {
        currentLevelCount = 0;

        if(onGameOver != null)
            onGameOver.Invoke();
    }
    
}
