﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeCollisionAdapter : MonoBehaviour
{
    // Public Variables
    public string collisionNameTag;
    public float colliderOffsetPosition;
    public float newColliderSize;
    [SerializeField]
    private Vector2 force;

    // Private Variables
    private bool isActive;
    private Quaternion originalScale;
    private Rigidbody2D rg2D;
    private BoxCollider2D knifeCollider;

    private void OnEnable()
    {
        isActive = true;
    }

    private void OnDisable()
    {
        isActive = false;
        ResetObjectData();
    }

    private void Awake()
    {
        rg2D = GetComponent<Rigidbody2D>();
        knifeCollider = GetComponent<BoxCollider2D>();

        originalScale = new Quaternion(gameObject.transform.localRotation.x, gameObject.transform.localRotation.y,
            gameObject.transform.localRotation.z, gameObject.transform.localRotation.w);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isActive)
        {
            Debug.Log("isActive : " + isActive);
            Debug.Log("Status : " + GameController.Instance.gameStatus);
            if(GameController.Instance.gameStatus == GameController.GameStatus.Playing)
            {
                rg2D.AddForce(force, ForceMode2D.Impulse);
                rg2D.gravityScale = 1;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!isActive)
            return;

        isActive = false;

        if(collision.collider.tag == collisionNameTag)
        {
            rg2D.bodyType = RigidbodyType2D.Kinematic;
            // TODO : play particle effect here
            rg2D.velocity = new Vector2(0, 0);
            rg2D.gravityScale = 0;

            this.transform.SetParent(collision.collider.transform);

            knifeCollider.offset = new Vector2(knifeCollider.offset.x, colliderOffsetPosition);
            knifeCollider.size = new Vector2(knifeCollider.size.x, newColliderSize);
        }
        else if (collision.collider.tag == "knife")
        {
            // TODO : play particle effect here
            rg2D.velocity = new Vector2(rg2D.velocity.x, -2);

            Vector3 dir = collision.contacts[0].point - new Vector2(transform.position.x, transform.position.y);
            dir = -dir.normalized;
            rg2D.AddForce(dir * force + new Vector2(10, 100));

            //TODO : send message to game controller, Game over
            //gameObject.GetComponent<Collider2D>().enabled = false;

            GameController.Instance.OnCollisionWithSameObject();
        }
    }

    private void ResetObjectData()
    {
       
        rg2D.velocity = new Vector2(0, 0);

        rg2D.bodyType = RigidbodyType2D.Dynamic;
        rg2D.gravityScale = 0;
        //this.transform.SetParent(collision.collider.transform);

        knifeCollider.offset = new Vector2(knifeCollider.offset.x, 0);
        knifeCollider.size = new Vector2(knifeCollider.size.x, 2.35f);

        gameObject.transform.localRotation = originalScale;
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
}
