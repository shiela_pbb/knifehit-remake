﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEventSender :MonoBehaviour {
    [Serializable]
    public class OnSendEventMessage {
        public string objectTag;
        public string gameobejctName;
        public string methodName;
        public string value;
    }

    public List<OnSendEventMessage> sendEventOnCollision;
    public List<OnSendEventMessage> sendEventOnTriggered;

    private void OnCollisionEnter2D(Collision2D collision) {
        if(sendEventOnCollision.Count == 0)
            return;

        for(int i = 0 ; i < sendEventOnCollision.Count ; i++) {
            if(collision.gameObject.tag == sendEventOnCollision[i].objectTag) {
                GameObject obj = GameObject.Find(sendEventOnCollision[i].gameobejctName);
                obj.SendMessage(sendEventOnCollision[i].methodName, sendEventOnCollision[i].value);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(sendEventOnTriggered.Count == 0)
            return;

        for(int i = 0 ; i < sendEventOnTriggered.Count ; i++) {
            if(collision.gameObject.tag == sendEventOnTriggered[i].objectTag) {
                GameObject obj = GameObject.Find(sendEventOnTriggered[i].gameobejctName);
                obj.SendMessage(sendEventOnTriggered[i].methodName);
            }
        }
    }
}
